attribute in vec3 pos;
attribute in vec3 normal;
attribute in vec2 tex_coord;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 model_matrix;


varying vec3 color;
void main(){
    gl_Position = vec4(pos, 1.0);
    color = (pos + 0.5);
}
