SETUP STEPS  
1.a Clone the repo  
1.b Create a build folder  
1.c Install CMake  

ON WINDOWS
2.Install Visual Studio 2017  
3.Run cmd (or other terminal like cmder) on the build directory  
4.Run the 'cmake -G "Visual Studio 15 2017" ..'  
5.Open GLFW.sln   
6.Change the shader path in iol-workshop.cpp to the correct path on your machine  
7.Set iol-workshop as the startup project  
8.Build and run  

ON MAC
(this wasn't fully tested on Mac, might need slight adjusts on class initializers)
2.Install Xcode  
3.Run cmd (or other terminal like cmder) on the build directory  
4.Run the 'cmake -G "Xcode" ..'  
5.Open GLFW.xproj   
6.Change the shader path in iol-workshop.cpp to the correct path on your machine  
7.Build and run  