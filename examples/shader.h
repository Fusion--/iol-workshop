#pragma once 
#include <string>
#include <glad/glad.h>

class Shader {
	
public:
	Shader(const char* vertex_shader, const char* fragment_shader);
	GLuint program_id;

private:
	std::string load_shader_file_(const char* shader_path);
	

};