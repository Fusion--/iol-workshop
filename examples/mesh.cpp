#include "mesh.h"
#include <string>

void ::_check_gl_error(const char * file, int line){
#ifdef _DEBUG
	GLenum err(glGetError());

	GLuint invalid_glstate_counter = 0;
	while (err != GL_NO_ERROR) {
		std::string error;

		
		switch (err) {
		case GL_INVALID_OPERATION: { error = "INVALID_OPERATION";   invalid_glstate_counter++;   break; }
		case GL_INVALID_ENUM:           error = "INVALID_ENUM";           break;
		case GL_INVALID_VALUE:          error = "INVALID_VALUE";          break;
		case GL_OUT_OF_MEMORY:          error = "OUT_OF_MEMORY";          break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:  error = "INVALID_FRAMEBUFFER_OPERATION";  break;
		}
#ifndef QUIET_GL_ERRORS
#if (defined _WIN32 || defined _WIN64 || defined __WINDOWS__)
		std::string error_message = error + " - " + file + ":" + std::to_string(line)+ "\n";
		std::wstring w_err(error_message.begin(),error_message.end());
		OutputDebugStringW(w_err.c_str());
#else
		std::cerr << "GL_" << error.c_str() << " - " << file << ":" << line << std::endl;
#endif
#endif
		err = glGetError();
		if (err == GL_NO_ERROR) invalid_glstate_counter = 0;
		if (invalid_glstate_counter > 100) break;
	}

	assert(invalid_glstate_counter == 0);
#endif
}


static std::vector<Vertex> create_quad_vertex(glm::vec3 min_point, glm::vec3 max_point) {
	std::vector<Vertex> vertex_array = {
		Vertex{ glm::vec3(min_point.x, max_point.y, min_point.z), glm::vec3(0.5773f,-0.5773f,0.5773f), glm::vec2(0.0f, 0.0f) },
		Vertex{ glm::vec3(min_point.x, min_point.y, min_point.z), glm::vec3(-0.5773f,-0.5773f,0.5773f), glm::vec2(0.0f, 1.0f) },
		Vertex{ glm::vec3(max_point.x, max_point.y, min_point.z), glm::vec3(0.5773f,0.5773f,0.5773f), glm::vec2(1.0f, 0.0f) },
		Vertex{ glm::vec3(max_point.x, min_point.y, min_point.z), glm::vec3(-0.5773f,0.5773f,0.5773f), glm::vec2(1.0f, 1.0f) }
	};
	return vertex_array;
}

std::vector<GLuint> create_quad_indices() {
	return { 0,1,2, 2,1,3 };
}

void Quad::upload_to_gpu(){
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	if (this->indices.size() != 0)
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

	glBindVertexArray(0);
}

Quad::Quad() : Quad(glm::vec3(-0.5,-0.5,0.0), glm::vec3(0.5, 0.5, 0.0)){
}

Quad::Quad(glm::vec3 min_point, glm::vec3 max_point) : 
	vertices(create_quad_vertex(min_point,max_point)),
	indices(create_quad_indices()) {
	upload_to_gpu();
}
