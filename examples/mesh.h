#include <glad/glad.h>
#include <vector>
#include <glm/glm/glm.hpp>

//Metafunction
void _check_gl_error(const char *file, int line);
//Error finding utility for opengl, that reports the line and file in which there's an error
#define check_gl_error() _check_gl_error(__FILE__,__LINE__)

typedef struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
} Vertex;


class Quad {
public:
	
	GLuint					vao, vbo, ebo;
	std::vector<Vertex>		vertices;
	std::vector<GLuint>		indices;

	Quad();
	Quad(glm::vec3 min_point, glm::vec3 max_point);
	void upload_to_gpu();
};