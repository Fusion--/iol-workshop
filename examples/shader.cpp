#include "shader.h"
#include <fstream>
#include <streambuf>
#include <assert.h>

std::string Shader::load_shader_file_(const char * shader_path){
	std::ifstream t(shader_path);
	std::string str;

	t.seekg(0, std::ios::end);
	str.reserve(t.tellg());
	t.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	return str;
}

Shader::Shader(const char * vertex_shader_path, const char * fragment_shader_path){
	std::string shader_string = load_shader_file_(vertex_shader_path);
	const char* v_shader = shader_string.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	GLint success;
	GLchar infoLog[4096];
	glShaderSource(vertex_shader, 1,&v_shader, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);

	if (success != GL_TRUE)
	{
		glGetShaderInfoLog(vertex_shader, 4096, NULL, infoLog);
		std::string log(infoLog);
#if (defined _WIN32 || defined _WIN64 || defined __WINDOWS__)
		std::wstring w_err(log.begin(), log.end());
		OutputDebugStringW(w_err.c_str());
#else
		std::cerr << "GL_" << log.c_str() << " - " << __FILE__ << ":" << __LINE__ << std::endl;
#endif
		assert(0);
	}

	shader_string = load_shader_file_(fragment_shader_path);
	const char* f_shader = shader_string.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &f_shader, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);

	if (success != GL_TRUE)
	{
		glGetShaderInfoLog(fragment_shader, 4096, NULL, infoLog);
		std::string log(infoLog);
#if (defined _WIN32 || defined _WIN64 || defined __WINDOWS__)
		std::wstring w_err(log.begin(), log.end());
		OutputDebugStringW(w_err.c_str());
#else
		std::cerr << "GL_" << log.c_str() << " - " << __FILE__ << ":" << __LINE__ << std::endl;
#endif
		assert(0);
	}

	program_id = glCreateProgram();
	glAttachShader(program_id, vertex_shader);
	glAttachShader(program_id, fragment_shader);
	glLinkProgram(program_id);

	glGetProgramiv(program_id, GL_LINK_STATUS, &success);
	if (success != GL_TRUE)
	{
		glGetProgramInfoLog(program_id, 512, NULL, infoLog);
		std::string log(infoLog);
		assert(0);
	}

#ifdef WIN32
	// For now we dont run this in MacOS. This check will fail because MacOS contexts are created without a draw buffer
	// (no DC) so this will throw an error. TODO: Solve the issue regarding no Draw buffer on MacOS or check if it  is 
	// really an issue.
	glValidateProgram(program_id);
	glGetProgramiv(program_id, GL_VALIDATE_STATUS, &success);
	if (success != GL_TRUE)
	{
		glGetProgramInfoLog(program_id, 512, NULL, infoLog);
		std::string log(infoLog);
		assert(0);
	}
#endif

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}