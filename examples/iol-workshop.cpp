#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "shader.h"
#include "object.h"

#include <stdlib.h>
#include <stdio.h>

#include <glm/glm/glm.hpp>

//Global variables
static GLFWwindow* window;
static float ratio;
static int width, height;

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

static void init_gl_context() {
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	window = glfwCreateWindow(640, 480, "Illusion of Life", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);

	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);
}

void render_function() {

}

int main(void)
{
	init_gl_context();
	Shader sh(	"C:\\Users\\jmartins.EXCHANGE\\Downloads\\workshop-glfw\\shaders\\normal.vert", 
				"C:\\Users\\jmartins.EXCHANGE\\Downloads\\workshop-glfw\\shaders\\normal.frag");
	Object obj;

    while (!glfwWindowShouldClose(window))
    {
        
        glm::mat4 m(1.0), p, mvp;

        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
		obj.Render(sh.program_id);
		check_gl_error();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}

