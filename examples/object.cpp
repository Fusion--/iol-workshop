#include "object.h"

void Object::Render(GLuint program_id){
	glUseProgram(program_id);
	glBindVertexArray(quad.vao);
	glDrawElements(GL_TRIANGLES, quad.indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
